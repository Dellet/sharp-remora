import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
import Header from './components/Header.vue';
import Dashboard from './components/pages/dashboard/Dashboard.vue';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const routes = [{
    name: 'Header',
    path: '/',
    component: Header
}, {
    name: 'Dashboard',
    path: '/dashboard',
    component: Dashboard
}];

const router = new VueRouter({ mode: 'history', routes: routes});
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
