<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sharp Remora</title>
        <link href="{{asset('animate.css/animate.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('glyphicons/glyphicons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('material-design-icons/material-design-icons.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('styles/app.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('styles/font.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="app" id="app">
        </div>
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                ]); ?>
        </script>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
