## About Sharp Remora

Help ordinary people make passive or primary income from trading and investment and capture a portion of their earnings.

## Development Environment

git clone https://bitbucket.org/Dellet/sharp-remora.git

cd sharp-remo

npm install

npm run watch

php artisan serve
...
